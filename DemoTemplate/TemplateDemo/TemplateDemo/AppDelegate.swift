//
//  AppDelegate.swift
//  TemplateDemo
//
//  Created by Admin on 26/03/21.
//

import UIKit
import TemplateSDK

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    static var sharedInstance: AppDelegate!

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        AppDelegate.sharedInstance = self
        
        UNUserNotificationCenter.current().delegate = self
        if TemplateUserDefaults.getNotificationStatus() != false {
            TemplateUtils.initNotifications(nil) { (res) in
                print("Init Notification Result: \(res)")
                
                (SliderViewController.sharedInstance?.childView as? TemplateSliderMenuViewController)?.table.reloadData()
            }
        }
        
        let rootModule = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController")
        let _ = setUpWindow([rootModule])
        
        return true
    }
    
    func setUpWindow(_ viewControllers: [UIViewController]) -> UIWindow? {
        let navigationController = UINavigationController()
        navigationController.viewControllers = viewControllers
        navigationController.setNavigationBarHidden(true, animated: false)
        
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return window
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // .. Receipt of device token
        let _ = TemplateUtils.getNotificationDeviceTokenFromData(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }


    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // handle error
        print(error)
    }
    
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Background Notification")
        print("Notification data: \(response.notification.request.content.userInfo)")
        
        completionHandler()
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Foreground Notification")
        
        completionHandler([.banner, .badge, .sound])
    }
}

