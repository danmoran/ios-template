//
//  TemplateScrollView.swift
//  TemplateSDK
//
//  Created by Admin on 19/04/21.
//

import Foundation
import UIKit

open class TemplateScrollView: UIScrollView, UITextFieldDelegate, UITextViewDelegate {
    open var fields: [UIView] = []
    open var ignoreNotifications: Bool = false
    open var scrollAnimationDuration: TimeInterval = 0.5
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    open override func awakeFromNib() {
        setup()
    }
    
    deinit {
        unRegisterForKeyboardNotifications()
    }
    
    open override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        guard newSuperview != nil else { return }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(assignTextDelegateForViewsBeneathView), object: self)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(assignTextDelegateForViewsBeneathView), object: self)
        
        self.perform(#selector(assignTextDelegateForViewsBeneathView), with: self, afterDelay: 0.1)
        
    }


    open func setup() {
        fields = []
        
        registerForNotifications()
    }

    open func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardAppear), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardDisappear), name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToActiveTextField), name: UITextField.textDidBeginEditingNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToActiveTextField), name: UITextView.textDidBeginEditingNotification, object: nil)
    }

    open func unRegisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UITextField.textDidBeginEditingNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UITextView.textDidBeginEditingNotification, object: nil)
    }

    @objc open func onKeyboardAppear(_ notification: NSNotification) {
        let info = notification.userInfo!
        let rect: CGRect = info[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
        let kbSize = rect.size

        let insets = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height, right: 0)
        self.contentInset = insets
        self.scrollIndicatorInsets = insets

        guard let superview = self.superview else {
            return
        }
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = superview.frame
        aRect.size.height -= kbSize.height
        
        if let activeField = fields.first(where: { $0.isFirstResponder }) {
            if !aRect.contains(activeField.frame.origin) {
                let scrollPoint = CGPoint(x: 0, y: activeField.frame.origin.y-kbSize.height)
                self.setContentOffset(scrollPoint, animated: true)
            }
        }
    }

    @objc open func onKeyboardDisappear(_ notification: NSNotification) {
        self.contentInset = UIEdgeInsets.zero
        self.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    @objc open func assignTextDelegateForViewsBeneathView(_ obj: Any) {
        func processWithView(_ view: UIView) {
            for childView in view.subviews {
                if childView is UITextField || childView is UITextView {
                    self.initializeView(childView)
                } else {
                    self.assignTextDelegateForViewsBeneathView(childView)
                }
            }
        }
        
        if let timer = obj as? Timer, let view = timer.userInfo as? UIView {
            processWithView(view)
        } else if let view = obj as? UIView {
            processWithView(view)
        }
    }
    
    @objc open func scrollToActiveTextField() {
        guard !ignoreNotifications, !fields.isEmpty else {
            return
        }
        
        ignoreNotifications = true
        
        let firstResponder = fields.first(where: { $0.isFirstResponder }) ?? fields[0]
        
        let visibleSpace = self.bounds.size.height - self.contentInset.top - self.contentInset.bottom
        
        let idealOffset = CGPoint(
            x: self.contentOffset.x,
            y: idealOffsetForView(firstResponder, withViewingAreaHeight: visibleSpace)
        )
        
        
        UIView.animate(withDuration: scrollAnimationDuration) {
            self.contentOffset = idealOffset
        } completion: { (_) in
            self.ignoreNotifications = false
        }
    }
    
    open func idealOffsetForView(_ view: UIView?, withViewingAreaHeight viewAreaHeight: CGFloat) -> CGFloat {
        let contentSize = self.contentSize
                
        var offset: CGFloat = 0.0
        let subviewRect = view != nil ? view!.convert(view!.bounds, to: self) : CGRect.zero
        
        var padding = (viewAreaHeight - subviewRect.height) / 2
        if padding < 20.0 {
            padding = 20.0
        }
        
        offset = subviewRect.origin.y - padding - self.contentInset.top
        
        if offset > (contentSize.height - viewAreaHeight) {
            offset = contentSize.height - viewAreaHeight
        }
        
        if offset < -self.contentInset.top {
            offset = -self.contentInset.top
        }
        
        return offset
    }
    
    open func initializeView(_ view: UIView) {
        if let textField = view as? UITextField, textField.delegate !== self {
            textField.delegate = self
            fields.append(textField)
        } else if let textView = view as? UITextView, textView.delegate !== self {
            textView.delegate = self
            fields.append(textView)
        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.returnKeyType == .next) {
            if !self.focusNextTextField(textField) {
                textField.resignFirstResponder()
                
            }
                
        } else {
            textField.resignFirstResponder()
            
        }
        
        return true
    }
    
    open func focusNextTextField(_ firstResponder: UIView) -> Bool {
        if let view = findNextInputViewAfterView(firstResponder) {
            view.becomeFirstResponder()
            return true
        }
        
        return false
    }
    
    open func findNextInputViewAfterView(_ firstResponder: UIView?) -> UIView? {
        if firstResponder == nil {
            if fields.isEmpty {
                return nil
            } else {
                return fields[0]
            }
        }
        
        var nextResponder: UIView?
        for (i, field) in fields.enumerated() {
            if field == firstResponder, i + 1 < fields.count, viewIsValidKeyViewCandidate(field) {
                nextResponder = fields[i + 1]
                break
            }
        }
        
        return nextResponder
    }
    
    open func viewIsValidKeyViewCandidate(_ view: UIView) -> Bool {
        if view.isHidden || !view.isUserInteractionEnabled { return false}
        
        if let textField = view as? UITextField, textField.isEnabled {
            return true
        }
        
        if let textView = view as? UITextView, textView.isEditable {
            return true
        }
        
        return false
    }
}
