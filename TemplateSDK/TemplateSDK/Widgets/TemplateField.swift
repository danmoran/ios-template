//
//  TemplateField.swift
//  TemplateSDK
//
//  Created by Admin on 29/03/21.
//

import Foundation
import UIKit

open class TemplateField: UITextField {
    @IBInspectable open var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable open var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable open var borderRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable open var setDefaultStyle: Bool {
        get {
            return _setDefaultStyle ?? false
        }
        set {
            _setDefaultStyle = newValue
            setUp()
        }
    }
    @IBInspectable open var setStyleHeight: Bool {
        get {
            return _setStyleHeight ?? false
        }
        set {
            _setStyleHeight = newValue
            setUp()
        }
    }
    
    private var _setDefaultStyle: Bool?
    private var _setStyleHeight: Bool?
    
    open var style: TemplateFieldStyle?
    
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setUp()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    public func setUp() {
        let tmpStyle = style ?? TemplateFieldStyle.defaultStyle
        
        if style != nil || setDefaultStyle {
            if let bgColor = tmpStyle.backgroundColor {
                self.backgroundColor = bgColor
            }
            
            if let borderColor = tmpStyle.borderColor {
                self.borderColor = borderColor
            }
            
            if let borderWidth = tmpStyle.borderWidth {
                self.borderWidth = borderWidth
            }
            
            if let borderRadius = tmpStyle.borderRadius {
                self.borderRadius = borderRadius
            }
            
            if let textColor = tmpStyle.textColor {
                self.textColor = textColor
            }
            
            if !self.isEnabled, let disabledTextColor = tmpStyle.disabledTextColor {
                self.textColor = disabledTextColor
            }
            
            if let tintColor = tmpStyle.tintColor {
                self.tintColor = tintColor
            }
            
            if setStyleHeight, let height = tmpStyle.height {
                if let constraint = self.constraints.filter({$0.firstAttribute == .height}).first {
                    constraint.constant = height
                } else {
                    self.heightAnchor.constraint(equalToConstant: height).isActive = true
                }
            }
        }
    }
}

public class TemplateFieldStyle {
    public var backgroundColor: UIColor?
    public var borderColor: UIColor?
    public var borderWidth: CGFloat?
    public var borderRadius: CGFloat?
    public var height: CGFloat?
    public var textColor: UIColor?
    public var disabledTextColor: UIColor?
    public var tintColor: UIColor?
    
    private static var _defaultStyle: TemplateFieldStyle?
    public static var defaultStyle: TemplateFieldStyle {
        get {
            let fld = _defaultStyle ?? TemplateFieldStyle()
            
            if _defaultStyle == nil {
                fld.backgroundColor = UIColor.white
                fld.textColor = UIColor.darkText
                fld.disabledTextColor = UIColor.darkGray
                fld.borderWidth = 1
                fld.borderColor = UIColor.lightGray
                fld.borderRadius = 6
                fld.height = 50
            }
            
            return fld
        }
        
        set {
            _defaultStyle = newValue
        }
    }
    
    public init() {}
}
