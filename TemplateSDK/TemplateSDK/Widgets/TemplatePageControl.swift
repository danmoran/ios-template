//
//  TemplatePageControl.swift
//  TemplateSDK
//
//  Created by Admin on 20/09/21.
//

import Foundation
import UIKit

open class TemplatePageControl: UIPageControl {
    
    @IBInspectable open var setDefaultStyle: Bool {
        get {
            return _setDefaultStyle ?? false
        }
        set {
            _setDefaultStyle = newValue
            setUp()
        }
    }
    
    private var _setDefaultStyle: Bool?
    open var style: TemplatePageControlStyle?
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setUp()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    open func setUp() {
        let _style = style ?? TemplateInstance.templateStyles.pageControlStyle ?? TemplatePageControlStyle.defaultStyle
        
        if style != nil || setDefaultStyle {
            if let bgColor = _style.backgroundColor {
                self.backgroundColor = bgColor
            }
            
            if let color = _style.nonSelectedIndicatorColor {
                pageIndicatorTintColor = color
            }
            
            if let color = _style.selectedIndicatorColor {
                currentPageIndicatorTintColor = color
            }
        }
    }
}

open class TemplatePageControlStyle {
    open var backgroundColor: UIColor?
    open var selectedIndicatorColor: UIColor?
    open var nonSelectedIndicatorColor: UIColor?
    
    private static var _defaultStyle: TemplatePageControlStyle?
    public static var defaultStyle: TemplatePageControlStyle {
        get {
            let btn = _defaultStyle ?? TemplatePageControlStyle()
            
            if _defaultStyle == nil {
                btn.backgroundColor = UIColor.clear
                btn.selectedIndicatorColor = UIColor.systemBlue
                btn.nonSelectedIndicatorColor = UIColor.white
            }
            
            return btn
        }

        set {
            _defaultStyle = newValue
        }
    }
    
    public init() {}
    
}
