//
//  TemplateButton.swift
//  TemplateSDK
//
//  Created by Admin on 26/03/21.
//

import Foundation
import UIKit

open class TemplateBorderlessButton: TemplateButton {
    open override var style: TemplateButtonStyle? {
        get {
            super.style ?? TemplateInstance.templateStyles.buttonBorderLessStyle
        }
        set {
            super.style = newValue
        }
    }
}

open class TemplateButton: UIButton {
    @IBInspectable public var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable public var borderRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable open var setDefaultStyle: Bool {
        get {
            return _setDefaultStyle ?? false
        }
        set {
            _setDefaultStyle = newValue
            setUp()
        }
    }
    @IBInspectable open var setStyleHeight: Bool {
        get {
            return _setStyleHeight ?? false
        }
        set {
            _setStyleHeight = newValue
            setUp()
        }
    }
    
    private var _setDefaultStyle: Bool?
    private var _setStyleHeight: Bool?
    
    open weak var delegate: TemplateButtonDelegate?
    open var style: TemplateButtonStyle?
    
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setUp()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    open func setUp() {
        let btnStyle = style ?? TemplateInstance.templateStyles.buttonStyle ?? TemplateButtonStyle.defaultStyle
        
        if style != nil || setDefaultStyle {
            if let bgColor = btnStyle.backgroundColor {
                self.backgroundColor = bgColor
            }
            
            if let borderColor = btnStyle.borderColor {
                self.borderColor = borderColor
            }
            
            if let borderWidth = btnStyle.borderWidth {
                self.borderWidth = borderWidth
            }
            
            if let borderRadius = btnStyle.borderRadius {
                self.borderRadius = borderRadius
            }
            
            if let textColor = btnStyle.textColor {
                setTitleColor(textColor, for: .normal)
            }
            
            if let font = btnStyle.font {
                titleLabel?.font = font
            }
            
            if setStyleHeight, let height = btnStyle.height {
                if let constraint = self.constraints.filter({$0.firstAttribute == .height}).first {
                    constraint.constant = height
                } else {
                    self.heightAnchor.constraint(equalToConstant: height).isActive = true
                }
            }
        }
        
        self.addTarget(self, action: #selector(self.clickHandler), for: .touchUpInside)
    }
    
    @objc func clickHandler(sender: TemplateButton) {
        delegate?.clickHandler(sender)
    }
}

public protocol TemplateButtonDelegate: AnyObject {
    func clickHandler(_ button: TemplateButton)
}

extension TemplateButtonDelegate {
    public func clickHandler(_ button: TemplateButton) {
        print("ClickHandler called and wasn't delegated")
    }
}

open class TemplateButtonStyle {
    open var backgroundColor: UIColor?
    open var borderColor: UIColor?
    open var borderWidth: CGFloat?
    open var borderRadius: CGFloat?
    open var height: CGFloat?
    open var textColor: UIColor?
    open var font: UIFont?
    
    private static var _defaultStyle: TemplateButtonStyle?
    public static var defaultStyle: TemplateButtonStyle {
        get {
            let btn = _defaultStyle ?? TemplateButtonStyle()
            
            if _defaultStyle == nil {
                btn.backgroundColor = UIColor.clear
                btn.textColor = UIColor.systemBlue
                btn.borderWidth = 1
                btn.borderColor = UIColor.systemBlue
                btn.borderRadius = 6
                btn.height = 50
                btn.font = UIFont.boldSystemFont(ofSize: 16)
            }
            
            return btn
        }

        set {
            _defaultStyle = newValue
        }
    }
    
    public init() {}
    
}
