//
//  AuthRecoveryViewController.swift
//  TemplateSDK
//
//  Created by Admin on 31/03/21.
//

import Foundation
import UIKit

public class AuthRecovery {
    public static var recoveryUrl: String!
    
    public static func loadFromNib(
        recoveryUrl: String? = nil,
        successRecoveryResult: ((_ success: Bool, _ result: Any?) -> Void)? = nil
    ) -> AuthRecoveryViewController {
        let vc = TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "AuthRecoveryViewController") as! AuthRecoveryViewController
        
        vc.successRecoveryResult = successRecoveryResult
        
        if let recoveryUrl = recoveryUrl {
            AuthRecovery.recoveryUrl = recoveryUrl
        }
        
        return vc
    }
}

open class AuthRecoveryUI: BaseViewController {
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var btnBack: TemplateButton?
    @IBOutlet weak var titleContainer: UIView?
    @IBOutlet weak var lblFieldEmail: UILabel?
    @IBOutlet weak var fieldEmail: TemplateField!
    @IBOutlet weak var btnRecover: TemplateButton!
    
}

public class AuthRecoveryViewController: BaseViewController {
    public var authRecoveryUI: AuthRecoveryUI!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var contentView: UIView!
    
    public var isEmailField: Bool = false
    public var fieldLabelColor: UIColor?
    
    public var successRecoveryResult: ((_ success: Bool, _ result: Any?) -> Void)? = nil
    
    public var enableLoadingView: Bool = true
    
    public var successMessage: String? = "A verification email to reset your password has been sent to your email address.  Please follow the directions in the email to complete your password reset"
    
    var isValidFields = true
    var fieldErrors: [(String, String)] = []
    
    public lazy var errorCallback: ((_ fieldName: String, _ errorMessage: String) -> Void) = {
        { (fieldName, errorMessage) in
            self.fieldErrors.append((fieldName, errorMessage))
            self.isValidFields = false
        }
    }()
    

    public lazy var emailValidator: Validator? = {
        let validator = Validator(text: "").nonEmpty()
        var fieldName = "Username"
        if isEmailField {
            let _ = validator.validEmail()
            fieldName = "Email"
        }
        
        return validator.addErrorCallback { (errorMessage) in
            self.errorCallback(fieldName, errorMessage)
        }
    }()
    
    public lazy var validators: [String: Validator] = {
        var validators: [String: Validator] = [:]
        
        validators["email"] = emailValidator
        
        
        return validators
    }()
    
    
    open override func loadView() {
        super.loadView()
        
        if authRecoveryUI == nil {
            authRecoveryUI = (TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "AuthRecoveryUI") as! AuthRecoveryUI)
        }
        
        authRecoveryUI.view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(authRecoveryUI.view)
        
        NSLayoutConstraint.activate([
            authRecoveryUI.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            authRecoveryUI.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            authRecoveryUI.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            authRecoveryUI.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        ])
    }
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
    
    public func setUpView() {
        if let title = authRecoveryUI.lblTitle?.text {
            self.title = title
        } else {
            self.title = "RECOVER"
        }
        removeBackButtonText()
        
        if isEmailField {
            authRecoveryUI.fieldEmail.placeholder = "Enter email"
            authRecoveryUI.fieldEmail.keyboardType = .emailAddress
            authRecoveryUI.fieldEmail.textContentType = .emailAddress
            authRecoveryUI.lblFieldEmail?.text = "Email"
        } else {
            authRecoveryUI.fieldEmail.placeholder = "Enter username"
            authRecoveryUI.fieldEmail.keyboardType = .default
            authRecoveryUI.fieldEmail.textContentType = .username
            authRecoveryUI.lblFieldEmail?.text = "Username"
        }
        
        if let fieldLabelColor = fieldLabelColor {
            authRecoveryUI.lblFieldEmail?.textColor = fieldLabelColor
        }

        authRecoveryUI.btnRecover.delegate = self
        authRecoveryUI.btnBack?.delegate = self
    }
    
    public func validateFields(_ showAlert: Bool = true) -> Bool {
        fieldErrors = []
        isValidFields = true
        
        if let emailValidator = emailValidator {
            let _ = authRecoveryUI.fieldEmail.setValidator(validator: emailValidator)
        }
        
        for (_, value) in validators {
            let _ = value.validate()
        }
        
        
        if !isValidFields, showAlert {
            TemplateUtils.showAlert(from: fieldErrors, presentIn: self)
        }
        
        return isValidFields
    }
    
    open func callRecovery<T: Codable>(type: T.Type) {
        if !validateFields() {
            return
        }

        if self.enableLoadingView {
            self.showIndicator()
        }
        
        TemplateUser.recovery(
            type: type,
            url: TemplateInstance.API_URL + AuthRecovery.recoveryUrl,
            email: authRecoveryUI.fieldEmail.text!,
            success: { result in
                print("Result = \(result)")

                if self.enableLoadingView {
                    self.hideIndicator()
                }
                
                if let successRecoveryResult = self.successRecoveryResult {
                    successRecoveryResult(true, result)
                } else if let message = self.successMessage ?? (result as? MessageResponse)?.message {
                    
                    let alert = UIAlertController(title: "Password Recovery", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            },
            failure: { message, error in
                if self.enableLoadingView {
                    self.hideIndicator()
                }
                
                if let message = message {
                    let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                } else if let error = error {
                    print(error)
                }
            }
        )
    }
    
    open func callRecovery() {
        callRecovery(type: MessageResponse.self)
    }
    
//    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "AuthRecoveryUISegue" {
//            authRecoveryUI = (segue.destination as! AuthRecoveryUI)
//
//            if let hideNavBar = self.hideNavBar {
//                authRecoveryUI.titleContainer?.isHidden = !hideNavBar
//            }
//        }
//    }
}

extension AuthRecoveryViewController: TemplateButtonDelegate {
    public func clickHandler(_ button: TemplateButton) {
        switch button {
        case authRecoveryUI.btnRecover:
            callRecovery()
            break
        case authRecoveryUI.btnBack:
            self.navigationController?.popViewController(animated: true)
            break
        default:
            break
        }
    }
}
