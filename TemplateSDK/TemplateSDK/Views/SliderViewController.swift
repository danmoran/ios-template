//
//  SliderViewController.swift
//  TemplateSDK
//
//  Created by Admin on 31/03/21.
//

import Foundation
import UIKit

public enum SliderSide {
    case left
    case right
}

public enum SliderWidthType {
    case percentage
    case fixed
}

public class SliderViewController: BaseViewController {
    private static var _sharedInstance: SliderViewController!
    
    public static var sharedInstance: SliderViewController! {
        get {
            if _sharedInstance == nil {
                _sharedInstance = (
                    TemplateUtils.mainStoryboard.instantiateViewController(
                        withIdentifier: "SliderViewController"
                    ) as! SliderViewController
                )
                
                _sharedInstance.initPosition()

                UIApplication.shared.keyWindow?.addSubview(_sharedInstance.view)
            }

            return _sharedInstance
        }
        set {
            _sharedInstance = newValue
        }
    }
    
    private var _setStyles: Bool = false
    
    public override var setStyles: Bool {
        get { return _setStyles }
        set { _setStyles = newValue }
    }
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var outsideView: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var contentLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var outsideToContentRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var outsideToParentRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var outsideToParentLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var outsideToContentLeftConstraint: NSLayoutConstraint!
    @IBOutlet private weak var closeButtonLeftConstraint: NSLayoutConstraint!
    @IBOutlet private weak var closeButtonRightConstraint: NSLayoutConstraint!
    
    public var sliderSide: SliderSide = .right { didSet { setUpView() }}
    public var sliderWidthType: SliderWidthType = .percentage { didSet { setUpView() } }
    public var sliderWidth: CGFloat = 80 {
        didSet {
            if sliderWidthType == .percentage {
                if sliderWidth > 100 {
                    sliderWidth = 100
                } else if sliderWidth < 0 {
                    sliderWidth = 0
                }
            }
            setUpView()
        }
    }
    public lazy var closeSliderImage: UIImage? = {
        UIImage(named: "icClose", in: TemplateUtils.getResourceBundle(), compatibleWith: nil)
    }() {
        didSet {
            if let sliderImage = closeSliderImage {
                closeButton.isHidden = false
                closeButton.setImage(sliderImage, for: .normal)
            } else {
                closeButton.isHidden = true
            }
        }
    }
    public var closeSliderSide: SliderSide = .right {
        didSet {
            if closeSliderSide == .left {
                closeButtonLeftConstraint.priority = .required
                closeButtonRightConstraint.priority = .defaultLow
            } else {
                closeButtonLeftConstraint.priority = .defaultLow
                closeButtonRightConstraint.priority = .required
            }
        }
    }
    private var isOpened = false
    
    private var screenSize: CGSize { get { UIScreen.main.bounds.size } }
    private var screenPoint: CGPoint { get { UIScreen.main.bounds.origin } }
    
    private var _childVC: UIViewController? {
        didSet {
            if let childVC = _childVC {
                addChild(childVC)
                childVC.view.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview(childVC.view)
                
                NSLayoutConstraint.activate([
                    childVC.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
                    childVC.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
                    childVC.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
                    childVC.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
                ])
                
                childVC.didMove(toParent: self)
            }
        }
    }
    
    public var childView: UIViewController? {
        set {
            _childVC?.willMove(toParent: nil)
            _childVC?.view.removeFromSuperview()
            _childVC?.removeFromParent()
            _childVC = newValue
        }
        get { _childVC }
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
    
    public func setEmbeddedView(_ viewController: UIViewController) {
        self.childView = viewController
    }
    
    public func instanceAndSetTemplateSliderMenu(
        defaultRows: [Any]? = nil,
        showLogout: Bool = true,
        showProfile: Bool = true,
        showHome:Bool = true,
        showNotifications: Bool = true,
        showPrivacyPolicy: Bool = true,
        showAbout: Bool = true,
        showShareApp: Bool = true,
        showCloseAccount: Bool = true
    ) -> TemplateSliderMenuViewController {
        let vc = TemplateSliderMenuViewController.loadFromNib(
            self,
            defaultRows,
            showLogout,
            showProfile,
            showHome,
            showNotifications,
            showPrivacyPolicy,
            showAbout,
            showShareApp,
            showCloseAccount
        )
        
        childView = vc
        
        return vc
    }

    public func setUpView() {
        var margin: CGFloat
        if sliderWidthType == .percentage {
            margin = self.view.frame.width - (self.view.frame.width * sliderWidth / 100)
        } else {
            let viewWidth = self.view.frame.width
            let difference = viewWidth - sliderWidth
            if difference < 0 {
                margin = 0
            } else if difference > viewWidth {
                margin = viewWidth
            } else {
                margin = difference
            }
        }

        if sliderSide == .left {
            contentLeftConstraint.constant = 0
            contentRightConstraint.constant = margin
            
            outsideToParentLeftConstraint.priority = .defaultLow
            outsideToContentRightConstraint.priority = .defaultLow
            outsideToParentRightConstraint.priority = .required
            outsideToContentLeftConstraint.priority = .required
        } else if sliderSide == .right {
            contentLeftConstraint.constant = margin
            contentRightConstraint.constant = 0
            
            outsideToParentLeftConstraint.priority = .required
            outsideToContentRightConstraint.priority = .required
            outsideToParentRightConstraint.priority = .defaultLow
            outsideToContentLeftConstraint.priority = .defaultLow
        }
        
        outsideView.addTarget(self, action: #selector(outsideViewClickHandler), for: .touchUpInside)
    }

    public func initPosition() {
        setXPosition(screenSize.width)
    }
    
    public func dummy() {
        
    }
    
    public func toggle(_ animate: Bool? = nil) {
        if isOpened {
            close(animate ?? false)
        } else {
            open(animate ?? true)
        }
    }
    
    public func open(_ animate: Bool = true) {
        if !isOpened {
            self.view.isHidden = false
            if sliderSide == .left {
                setXPosition(screenPoint.x - self.view.frame.width)
                
                if !animate {
                    setXPosition(screenPoint.x)
                    outsideView.isHidden = false
                } else {
                    UIView.animate(withDuration: 1.0) {
                        self.setXPosition(self.screenPoint.x)
                    } completion: { (_) in
                        self.outsideView.isHidden = false
                    }
                }
            } else if sliderSide == .right {
                setXPosition(screenSize.width)
                
                if !animate {
                    setXPosition(screenSize.width - self.view.frame.width)
                    self.outsideView.isHidden = false
                } else {
                    UIView.animate(withDuration: 1.0) {
                        self.setXPosition(self.screenSize.width - self.view.frame.width)
                    } completion: { (_) in
                        self.outsideView.isHidden = false
                    }
                }
            }
            
            isOpened = true
        }
    }
    
    public func close(_ animate: Bool = false) {
        if isOpened {
            self.outsideView.isHidden = true
            
            if sliderSide == .left {
                setXPosition(screenPoint.x)
                
                if !animate {
                    setXPosition(screenPoint.x - self.view.frame.width)
                    self.view.isHidden = true
                } else {
                    UIView.animate(withDuration: 1.0) {
                        self.setXPosition(self.screenPoint.x - self.view.frame.width)
                    } completion: { (_) in
                        self.view.isHidden = true
                    }
                }
            } else if sliderSide == .right {
                setXPosition(screenSize.width - self.view.frame.width)
                
                if !animate {
                    setXPosition(screenSize.width)
                } else {
                    UIView.animate(withDuration: 1.0) {
                        self.setXPosition(self.screenSize.width)
                    } completion: { (_) in
                        self.view.isHidden = true
                    }
                }
            }
            
            isOpened = false
        }
    }
    
    private func setXPosition(_ pos: CGFloat) {
        self.view.frame.origin.x = pos
    }
    
    @objc @IBAction func outsideViewClickHandler(_ view: UIButton) {
        close(true)
    }
}
