//
//  AuthSignUpViewController.swift
//  TemplateSDK
//
//  Created by Admin on 31/03/21.
//

import Foundation
import UIKit

public class AuthSignUp {
    public static var signUpUrl: String!
    
    public static func loadFromNib(
        signUpUrl: String? = nil,
        successSignUpResult: ((_ success: Bool, _ result: Any?) -> UIViewController?)? = nil
    ) -> AuthSignUpViewController {
        let vc = TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "AuthSignUpViewController") as! AuthSignUpViewController
        
        vc.successSignUpResult = successSignUpResult
        
        if let signUpUrl = signUpUrl {
            AuthSignUp.signUpUrl = signUpUrl
        }
        
        return vc
    }
}

open class AuthSignUpUI: BaseViewController {
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var btnBack: TemplateButton?
    @IBOutlet weak var titleContainer: UIView?
    @IBOutlet weak var lblFieldName: UILabel?
    @IBOutlet weak var fieldName: TemplateField!
    @IBOutlet weak var lblFieldUsername: UILabel?
    @IBOutlet weak var fieldUsername: TemplateField!
    @IBOutlet weak var lblFieldPassword: UILabel?
    @IBOutlet weak var fieldPassword: UITextField!
    
    @IBOutlet weak var btnSignUp: TemplateButton!
    @IBOutlet weak var btnGoToLogin: TemplateButton?
}


open class AuthSignUpViewController: BaseViewController {
    open var authSignUpUI: AuthSignUpUI!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var contentView: UIView!
    
    open var isEmailField: Bool = false
    open var fieldLabelColor: UIColor?
    open var clickLabelColor: UIColor?
    open var enableGoToLogin: Bool = true
    
    open var successSignUpResult: ((_ success: Bool, _ result: Any?) -> UIViewController?)? = nil
    open var storeUser: Bool = true
    
    open var enableLoadingView: Bool = true
    
    var isValidFields = true
    var fieldErrors: [(String, String)] = []
    
    open lazy var errorCallback: ((_ fieldName: String, _ errorMessage: String) -> Void) = {
        { (fieldName, errorMessage) in
            self.fieldErrors.append((fieldName, errorMessage))
            self.isValidFields = false
        }
    }()
    
    open lazy var nameValidator: Validator? = {
        Validator(text: "").nonEmpty().addErrorCallback{ (errorMessage) in
            self.errorCallback("Name", errorMessage)
        }
    }()
    open lazy var usernameValidator: Validator? = {
        let validator = Validator(text: "").nonEmpty()
        var fieldName = "Username"
        if isEmailField {
            let _ = validator.validEmail()
            fieldName = "Email"
        }
        
        return validator.addErrorCallback { (errorMessage) in
            self.errorCallback(fieldName, errorMessage)
        }
    }()
    open lazy var passwordValidator: Validator? = {
        Validator(text: "").nonEmpty().addErrorCallback{ (errorMessage) in
            self.errorCallback("Password", errorMessage)
        }
    }()
    
    open lazy var validators: [String: Validator] = {
        var validators: [String: Validator] = [:]
        
        validators["name"] = nameValidator
        validators["username"] = usernameValidator
        validators["password"] = passwordValidator
        
        
        return validators
    }()
    
    
    open override func loadView() {
        super.loadView()
        
        if authSignUpUI == nil {
            authSignUpUI = (TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "AuthSignUpUI") as! AuthSignUpUI)
        }
        
        authSignUpUI.view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(authSignUpUI.view)
        
        NSLayoutConstraint.activate([
            authSignUpUI.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            authSignUpUI.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            authSignUpUI.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            authSignUpUI.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        ])
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
    
    open func setUpView() {
        if let title = authSignUpUI.lblTitle?.text {
            self.title = title
        } else {
            self.title = "SIGN UP"
        }
        removeBackButtonText()
        
        if let hideNavBar = self.hideNavBar {
            authSignUpUI.titleContainer?.isHidden = !hideNavBar
        }
        
        if isEmailField {
            authSignUpUI.fieldUsername.placeholder = "EMAIL"
            authSignUpUI.fieldUsername.keyboardType = .emailAddress
            authSignUpUI.fieldUsername.textContentType = .emailAddress
            authSignUpUI.lblFieldUsername?.text = "Email"
        } else {
            authSignUpUI.fieldUsername.placeholder = "USERNAME"
            authSignUpUI.fieldUsername.keyboardType = .default
            authSignUpUI.fieldUsername.textContentType = .username
            authSignUpUI.lblFieldUsername?.text = "Username"
        }

        if let fieldLabelColor = fieldLabelColor {
            authSignUpUI.lblFieldName?.textColor = fieldLabelColor
            authSignUpUI.lblFieldUsername?.textColor = fieldLabelColor
            authSignUpUI.lblFieldPassword?.textColor = fieldLabelColor
        }

        if let clickLabelColor = clickLabelColor {
            authSignUpUI.btnGoToLogin?.setTitleColor(clickLabelColor, for: .normal)
        }

        authSignUpUI.btnGoToLogin?.isHidden = !enableGoToLogin
        
        authSignUpUI.btnSignUp.delegate = self
        authSignUpUI.btnGoToLogin?.delegate = self
        authSignUpUI.btnBack?.delegate = self
    }
    
    open func validateFields(_ showAlert: Bool = true) -> Bool {
        fieldErrors = []
        isValidFields = true
        
        if let nameValidator = nameValidator {
            let _ = authSignUpUI.fieldName.setValidator(validator: nameValidator)
        }
        
        if let usernameValidator = usernameValidator {
            let _ = authSignUpUI.fieldUsername.setValidator(validator: usernameValidator)
        }
        
        if let passwordValidator = passwordValidator {
            let _ = authSignUpUI.fieldPassword.setValidator(validator: passwordValidator)
        }
        
        for (_, value) in validators {
            let _ = value.validate()
        }
        
        
        if !isValidFields, showAlert {
            TemplateUtils.showAlert(from: fieldErrors, presentIn: self)
        }
        
        return isValidFields
    }
    
    open func callSignUp<T: Codable>(type: T.Type) {
        if !validateFields() {
            return
        }
        
        if self.enableLoadingView {
            self.showIndicator()
        }
        
        TemplateUser.signUp(
            type: type,
            url: TemplateInstance.API_URL + AuthSignUp.signUpUrl,
            name: authSignUpUI.fieldName.text!,
            email: authSignUpUI.fieldUsername.text!,
            password: authSignUpUI.fieldPassword.text!,
            deviceToken: TemplateUserDefaults.sharedInstance.deviceToken, // TODO: get device token
            success: { user in
                
                if self.storeUser {
                    do { try TemplateUserDefaults.setUser(user: user) }
                    catch { print("Store user failed") }
                }
                
                if self.enableLoadingView {
                    self.hideIndicator()
                }
                UserDefaults.standard.set(nil, forKey: "isSocialLogin")
                if let successSignUpResult = self.successSignUpResult, let vc = successSignUpResult(true, user) {
                    self.navigationController?.setViewControllers([vc], animated: true)
                } else {
                    self.navigationController?.setViewControllers(
                        [TemplateInstance.instanceTutorialVC?() ?? TemplateDashboard.loadFromNib()],
                        animated: true
                    )
                }
            },
            failure: { message, error in
                if self.enableLoadingView {
                    self.hideIndicator()
                }
                
                if let message = message {
                    let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                } else if let error = error {
                    print(error)
                }
            }
        )
    }
    
    open func callSignUp() {
        callSignUp(type: TemplateUser.self)
    }
    
//    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "AuthSignUpUISegue" {
//            authSignUpUI = (segue.destination as! AuthSignUpUI)
//
//            if let hideNavBar = self.hideNavBar {
//                authSignUpUI.titleContainer?.isHidden = !hideNavBar
//            }
//        }
//    }
    
}

extension AuthSignUpViewController: TemplateButtonDelegate {
    public func clickHandler(_ button: TemplateButton) {
        switch button {
        case authSignUpUI.btnSignUp:
            callSignUp()
            break
        case authSignUpUI.btnGoToLogin:
            navigationController?.pushViewController(
                TemplateInstance.instanceLoginVC?() ?? AuthLogin.loadFromNib(),
                animated: true
            )
            break
        case authSignUpUI.btnBack:
            navigationController?.popViewController(animated: true)
            break
        default:
            break
        }
    }
}
