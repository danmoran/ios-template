//
//  AuthSelectorViewController.swift
//  TemplateSDK
//
//  Created by Admin on 29/03/21.
//

import Foundation
import UIKit

open class AuthSelectorViewController: BaseViewController {
    @IBOutlet weak var viewBackgroundImage: UIImageView!
    @IBOutlet weak var btnLogin: TemplateButton!
    @IBOutlet weak var btnSignUp: TemplateButton!
    @IBOutlet weak var viewLogoImage: UIImageView!
    
    open var backgroundImage: UIImage?
    open var backgroundColor: UIColor?
    open var logoImage: UIImage?
    
    open class func loadFromNib() -> AuthSelectorViewController {
        return TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "AuthSelectorViewController") as! AuthSelectorViewController
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        SliderViewController.sharedInstance.dummy()
        
        setUpView()
    }
    
    
    open func setUpView() {
        removeBackButtonText()
        
        if let backgroundImage = backgroundImage {
            viewBackgroundImage.image = backgroundImage
        }
        
        if let backgroundColor = backgroundColor {
            viewBackgroundImage.backgroundColor = backgroundColor
        }
        
        if let logoImage = logoImage {
            viewLogoImage.image = logoImage
        }
        
        btnLogin.delegate = self
        btnSignUp.delegate = self
    }
    
}

extension AuthSelectorViewController: TemplateButtonDelegate {
    open func clickHandler(_ button: TemplateButton) {
        switch button {
        case btnLogin:
            self.navigationController?.pushViewController(
                TemplateInstance.instanceLoginVC?() ?? AuthLogin.loadFromNib(),
                animated: true
            )
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        case btnSignUp:
            self.navigationController?.pushViewController(
                TemplateInstance.instanceSignUpVC?() ?? AuthSignUp.loadFromNib(),
                animated: true
            )
        default:
            break
        }
    }
}
