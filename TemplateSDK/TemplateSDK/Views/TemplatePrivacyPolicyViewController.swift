//
//  TemplatePrivacyPolicy.swift
//  TemplateSDK
//
//  Created by Admin on 20/04/21.
//

import Foundation
import UIKit


open class TemplatePrivacyPolicy {
    
    public static func loadFromNib(
        privacyPolicyText: String? = nil,
        isHtmlText: Bool? = nil
    ) -> TemplatePrivacyPolicyViewController {
        let vc = TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "TemplatePrivacyPolicyViewController") as! TemplatePrivacyPolicyViewController
        
        if let isHtmlText = isHtmlText {
            vc.isHtmlText = isHtmlText
        }
        
        if let privacyPolicyText = privacyPolicyText {
            vc.privacyPolicyText = privacyPolicyText
        }
        
        return vc
    }
}

open class TemplatePrivacyPolicyUI: BaseViewController {
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var btnBack: TemplateButton?
    @IBOutlet weak var titleContainer: UIView?
    @IBOutlet weak var textViewPrivacyPolicy: UITextView?
    
}

open class TemplatePrivacyPolicyViewController: BaseViewController {
    open  var  templatePrivacyPolicyUI: TemplatePrivacyPolicyUI!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var contentView: UIView!
    
    open var privacyPolicyText: String = "" {
        didSet {
            setUpPrivacyPolicyText()
        }
    }
    open var isHtmlText: Bool = false
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
    
    open override func loadView() {
        super.loadView()
        if templatePrivacyPolicyUI == nil {
            templatePrivacyPolicyUI = (TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "TemplatePrivacyPolicyUI") as! TemplatePrivacyPolicyUI)
        }
        for view in contentView.subviews {
            view.removeFromSuperview()
        }
        templatePrivacyPolicyUI.view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(templatePrivacyPolicyUI.view)
        
        NSLayoutConstraint.activate([
            templatePrivacyPolicyUI.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            templatePrivacyPolicyUI.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            templatePrivacyPolicyUI.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            templatePrivacyPolicyUI.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        ])
    }
    
    open func setUpView() {
        guard templatePrivacyPolicyUI != nil else {
            return
        }
        
        if(templatePrivacyPolicyUI.lblTitle != nil) {
            self.title = templatePrivacyPolicyUI.lblTitle?.text
        } else {
            self.title = "PRIVACY POLICY"
        }
        
        
        setUpPrivacyPolicyText()
        
        templatePrivacyPolicyUI.btnBack?.delegate = self
    }
    
    open func setUpPrivacyPolicyText() {
        guard templatePrivacyPolicyUI != nil else {
            return
        }
        
        if !isHtmlText {
            templatePrivacyPolicyUI.textViewPrivacyPolicy?.text = privacyPolicyText
        } else {
            templatePrivacyPolicyUI.textViewPrivacyPolicy?.attributedText = getPrivacyPolicyAsHtml()
        }
    }
    
    open func getPrivacyPolicyAsHtml() -> NSAttributedString? {
        if let htmlData = privacyPolicyText.data(using: .unicode) {
            do {
                return try NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            } catch {
                print(error)
            }
        }
        
        return nil
    }
    
//    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "TemplatePrivacyPolicyUISegue" {
//            templatePrivacyPolicyUI = (segue.destination as! TemplatePrivacyPolicyUI)
//
//            if let hideNavBar = self.hideNavBar {
//                templatePrivacyPolicyUI.titleContainer?.isHidden = !hideNavBar
//            }
//        }
//    }
}

extension TemplatePrivacyPolicyViewController: TemplateButtonDelegate {
    open func clickHandler(_ button: TemplateButton) {
        if button == templatePrivacyPolicyUI.btnBack {
            navigationController?.popViewController(animated: true)
        }
    }
}
