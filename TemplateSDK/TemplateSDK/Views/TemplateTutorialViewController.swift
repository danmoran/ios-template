//
//  TemplateTutorialViewController.swift
//  TemplateSDK
//
//  Created by Admin on 20/09/21.
//

import Foundation
import UIKit


public struct TemplateTutorial {
    public static func loadFromNib(dataSource: TemplateTutorialDataSource) -> TemplateTutorialViewController {
        let vc = TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "TemplateTutorialViewController") as! TemplateTutorialViewController
        
        vc.dataSource = dataSource
        
        return vc
    }
}

public protocol TemplateTutorialDataSource: AnyObject {
    func getPageCount() -> Int
    
    func getPageView(_ position: Int) -> UIView
    
    func skipDidSelect()
    
    func doneDidSelect()
}

open class TemplateTutorialViewController: BaseViewController {
    @IBOutlet weak var btnSkip: TemplateBorderlessButton!
    @IBOutlet weak var btnNext: TemplateBorderlessButton!
    @IBOutlet weak var pageControl: TemplatePageControl!
    @IBOutlet weak var contentView: UIView!
    
    open weak var dataSource: TemplateTutorialDataSource? {
        didSet {
            if let count = dataSource?.getPageCount(), count > 1 {
                btnNext?.isHidden = false
                pageControl?.isHidden = false
                pageControl?.numberOfPages = count
            }
        }
    }
    open var currentPosition: Int = 0
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        reloadPageContentView()
        
        guard let dataSource = dataSource, dataSource.getPageCount() > 1 else {
            btnNext.isHidden = true
            pageControl.isHidden = true
            return
        }
        
        pageControl.numberOfPages = dataSource.getPageCount()
    }
    
    @IBAction func btnSkipAction() {
        self.navigationController?.setViewControllers(
            [TemplateInstance.instanceDashboardVC?() ?? TemplateDashboard.loadFromNib()],
            animated: true)
        dataSource?.skipDidSelect()
    }
    
    @IBAction func btnNextAction() {
        guard let dataSource = dataSource else {
            return
        }
        
        
        if currentPosition + 1 < dataSource.getPageCount()  {
            currentPosition += 1
            
            if currentPosition + 1 == dataSource.getPageCount() {
                btnNext.setTitle("DONE", for: .normal)
            }
            
            reloadPageContentView()
        } else {
            self.navigationController?.setViewControllers(
                [TemplateInstance.instanceDashboardVC?() ?? TemplateDashboard.loadFromNib()],
                animated: true)
            dataSource.doneDidSelect()
        }
    }
    
    @IBAction func pageControlPageAction() {
        guard let dataSource = dataSource else {
            return
        }
        currentPosition = pageControl.currentPage
        
        if currentPosition + 1 == dataSource.getPageCount() {
            btnNext.setTitle("DONE", for: .normal)
        } else {
            btnNext.setTitle("NEXT", for: .normal)
        }
        reloadPageContentView()
        
    }
    
    open func reloadPageContentView() {
        guard let dataSource = dataSource else {
            return
        }
        
        pageControl.currentPage = currentPosition
        let pageView = dataSource.getPageView(currentPosition)
        pageView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.subviews.forEach({$0.removeFromSuperview()})
        contentView.addSubview(pageView)
        
        NSLayoutConstraint.activate([
            pageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            pageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            pageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            pageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
//            pageView.heightAnchor.constraint(equalToConstant: 200)
        ])
    }
}
