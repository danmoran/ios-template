//
//  TemplateAboutViewController.swift
//  TemplateSDK
//
//  Created by Admin on 20/04/21.
//

import Foundation
import UIKit


open class TemplateAbout {
    
    public static func loadFromNib(
        aboutText: String? = nil,
        isHtmlText: Bool? = nil
    ) -> TemplateAboutViewController {
        let vc = TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "TemplateAboutViewController") as! TemplateAboutViewController
        
        if let isHtmlText = isHtmlText {
            vc.isHtmlText = isHtmlText
        }
        
        if let aboutText = aboutText {
            vc.aboutText = aboutText
        }
        
        return vc
    }
}

open class TemplateAboutUI: BaseViewController {
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var btnBack: TemplateButton?
    @IBOutlet weak var titleContainer: UIView?
    @IBOutlet weak var textViewAbout: UITextView?
    @IBOutlet weak var textViewAboutTopConstraint: NSLayoutConstraint?
    @IBOutlet weak var textViewAboutTopParentConstraint: NSLayoutConstraint?
    
    open override var hideNavBar: Bool? {
        didSet {
            setTitleContainerVisibility()
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        setTitleContainerVisibility()
    }
    
    open func setTitleContainerVisibility() {
        if let hideNavBar = hideNavBar {
            titleContainer?.isHidden = !hideNavBar
            
            if hideNavBar {
                textViewAboutTopConstraint?.priority = .required
                textViewAboutTopParentConstraint?.priority = .defaultLow
            } else {
                textViewAboutTopConstraint?.priority = .defaultLow
                textViewAboutTopParentConstraint?.priority = .required
            }
        }
    }
}

open class TemplateAboutViewController: BaseViewController {
    open  var templateAboutUI: TemplateAboutUI!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var contentView: UIView!
    
    open var aboutText: String = "" {
        didSet {
            setUpAboutText()
        }
    }
    open var isHtmlText: Bool = false
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
    
    open override func loadView() {
        super.loadView()
        if templateAboutUI == nil {
            templateAboutUI = (TemplateUtils.mainStoryboard.instantiateViewController(withIdentifier: "TemplateAboutUI") as! TemplateAboutUI)
        }
        for view in contentView.subviews {
            view.removeFromSuperview()
        }
        templateAboutUI.view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(templateAboutUI.view)
        
        NSLayoutConstraint.activate([
            templateAboutUI.view.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            templateAboutUI.view.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            templateAboutUI.view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            templateAboutUI.view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        ])
    }
    
    
    open func setUpView() {
        guard templateAboutUI != nil else {
            return
        }
        if(templateAboutUI.lblTitle != nil) {
            self.title = templateAboutUI.lblTitle?.text
        } else {
            self.title = "ABOUT US"
        }
        
        
        setUpAboutText()
        
        templateAboutUI.btnBack?.delegate = self
    }
    
    open func setUpAboutText() {
        guard templateAboutUI != nil else {
            return
        }
        
        if !isHtmlText {
            templateAboutUI.textViewAbout?.text = aboutText
        } else {
            templateAboutUI.textViewAbout?.attributedText = getAboutAsHtml()
        }
    }
    
    open func getAboutAsHtml() -> NSAttributedString? {
        if let htmlData = aboutText.data(using: .unicode) {
            do {
                return try NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            } catch {
                print(error)
            }
        }
        
        return nil
    }
    
//    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "TemplateAboutUISegue" {
//            templateAboutUI = (segue.destination as! TemplateAboutUI)
//
//            if let hideNavBar = self.hideNavBar {
//                templateAboutUI.hideNavBar = hideNavBar
//            }
//        }
//    }
}

extension TemplateAboutViewController: TemplateButtonDelegate {
    open func clickHandler(_ button: TemplateButton) {
        if button == templateAboutUI.btnBack {
            navigationController?.popViewController(animated: true)
        }
    }
}
