//
//  MessageResponse.swift
//  TemplateSDK
//
//  Created by Admin on 31/03/21.
//

import Foundation

public class MessageResponse: Codable {
    public var message: String
    
    private enum CodingKeys: String, CodingKey {
        case message
    }
}
