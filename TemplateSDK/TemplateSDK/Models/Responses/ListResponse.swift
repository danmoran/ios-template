//
//  ListResponse.swift
//  TemplateSDK
//
//  Created by Admin on 22/09/21.
//

import Foundation

public struct ListResponse<T: Decodable>: Decodable {
    public var data: [T]
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
}
