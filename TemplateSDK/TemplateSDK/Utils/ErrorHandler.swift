//
//  ErrorHandler.swift
//  TemplateSDK
//
//  Created by Admin on 30/03/21.
//

import Foundation

public class ErrorHandler {
    
    public static func getErrorMessage(_ data: Data) -> String {
        return getErrorMessageOrNil(data) ?? "An error occurred"
    }
    
    public static func getErrorMessageOrNil(_ data: Data) -> String? {
        if let errorResponse = try? JSONDecoder().decode(ErrorResponse.self, from: data) {
            return errorResponse.toString()
        }
        
        return nil
    }
}

public struct ErrorResponse: Decodable {
    public var stringMessage: String?
    public var multipleMessages: Dictionary<String, [String]?>?
    public var multipleStringMessages: Dictionary<String, String?>?
    
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        if let stringMessage = try? values.decode(String.self, forKey: .message) {
            self.stringMessage = stringMessage
        } else if let multipleMessages = try? values.decode(Dictionary<String, [String]?>.self, forKey: .message) {
            self.multipleMessages = multipleMessages
        } else if let multipleStringMessages = try? values.decode(Dictionary<String, String?>.self, forKey: .message) {
            self.multipleStringMessages = multipleStringMessages
        } else {
            throw NSError(domain: "Error response not handled", code: 800, userInfo: nil)
        }
    }
    
    public func toString() -> String {
        if let message = stringMessage {
            return message
        } else if let messages = multipleMessages {
            var message = ""
            for (index, row) in messages.enumerated() {
                var value = ""
                
                if let rowValues = row.value {
                    for (rowValueIndex, rowValue) in rowValues.enumerated() {
                        value += rowValue
                        
                        if rowValueIndex + 1 < rowValues.count {
                            value += ", "
                        }
                    }
                }
                
                message += "\(row.key): \(value)"
                
                if index + 1 < messages.count {
                    message += "\n"
                }
            }
            
            return message
        } else if let messagesString = multipleStringMessages {
            var message = ""
            for (index, row) in messagesString.enumerated() {
                message += "\(row.key): \(row.value ?? "")"
                
                if index + 1 < messagesString.count {
                    message += "\n"
                }
            }
            
            return message
        }
        
        return "Request Response Error"
    }
    
    private enum CodingKeys: String, CodingKey {
        case message
    }
}
