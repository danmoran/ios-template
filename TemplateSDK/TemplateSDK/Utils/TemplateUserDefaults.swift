//
//  TemplateUserDefaults.swift
//  TemplateSDK
//
//  Created by Admin on 14/04/21.
//

import Foundation
import UIKit

open class TemplateUserDefaults {
    public static var sharedInstance: TemplateUserDefaults = TemplateUserDefaults()
    
    open lazy var deviceToken: String? = {
        return TemplateUserDefaults.getNotificationDeviceToken()
    }()
    
    open lazy var user: TemplateUser? = {
        do { return try TemplateUserDefaults.getTemplateUser() }
        catch { return nil }
    }()
    
    open func setUser<T: Codable>(user: T) throws {
        if let user = user as? TemplateUser {
            if user.token == nil {
                user.token = TemplateUserDefaults.sharedInstance.user?.token
            }
            TemplateUserDefaults.sharedInstance.user = user
        }
        
        let encodedUser: Data = try JSONEncoder().encode(user)
        UserDefaults.standard.set(encodedUser, forKey: "templateUser")
    }
    
    open class func setUser<T: Codable>(user: T) throws {
        return try TemplateUserDefaults.sharedInstance.setUser(user: user)
    }
    
    open func getUser<T: Codable>(userType: T.Type) throws -> T? {
        if let userData = UserDefaults.standard.data(forKey: "templateUser") {
            return try JSONDecoder().decode(userType, from: userData)
        }
        
        return nil
    }
    
    open class func getUser<T: Codable>(userType: T.Type) throws -> T? {
        return try TemplateUserDefaults.sharedInstance.getUser(userType: userType)
    }
    
    open func setTemplateUser(user: TemplateUser) throws {
        try setUser(user: user)
    }
    
    open class func setTemplateUser(user: TemplateUser) throws {
        return try TemplateUserDefaults.sharedInstance.setTemplateUser(user: user)
    }
    
    open func getTemplateUser() throws -> TemplateUser? {
        return try getUser(userType: TemplateUser.self)
    }
    
    open class func getTemplateUser() throws -> TemplateUser? {
        return try TemplateUserDefaults.sharedInstance.getTemplateUser()
    }
    
    open func removeUser() {
        TemplateUserDefaults.sharedInstance.user?.token = ""
        UserDefaults.standard.removeObject(forKey: "templateUser")
    }
    
    open class func removeUser() {
        TemplateUserDefaults.sharedInstance.removeUser()
    }
    
    open func setNotificationDeviceToken(deviceToken: String) {
        UserDefaults.standard.set(deviceToken, forKey: "templateDeviceToken")
    }
    
    open class func setNotificationDeviceToken(deviceToken: String) {
        TemplateUserDefaults.sharedInstance.setNotificationDeviceToken(deviceToken: deviceToken)
    }
    
    open func getNotificationDeviceToken() -> String? {
        return UserDefaults.standard.string(forKey: "templateDeviceToken")
    }
    
    open class func getNotificationDeviceToken() -> String? {
        return TemplateUserDefaults.sharedInstance.getNotificationDeviceToken()
    }
    
    open func setNotificationStatus(enabled: Bool) {
        UserDefaults.standard.set(enabled, forKey: "templateNotificationEnabled")
    }
    
    open class func setNotificationStatus(enabled: Bool) {
        TemplateUserDefaults.sharedInstance.setNotificationStatus(enabled: enabled)
    }
    
    open func getNotificationStatus() -> Bool? {
        return UserDefaults.standard.object(forKey: "templateNotificationEnabled") as? Bool
    }
    
    open class func getNotificationStatus() -> Bool? {
        return TemplateUserDefaults.sharedInstance.getNotificationStatus()
    }
    
    open class func isUserLoggedIn() -> Bool {
        return sharedInstance.user?.token?.isEmpty == false
    }
}
