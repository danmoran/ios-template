//
//  TemplateUtils.swift
//  TemplateSDK
//
//  Created by Admin on 29/03/21.
//

import Foundation
import UIKit
import MBProgressHUD


public class TemplateUtils {
    
    private init() {}
    
    public static let mainStoryboard = UIStoryboard.init(name: "TemplateSDKStoryboard", bundle: resourceBundle)
    
    public static let resourceBundle = getResourceBundle()
    
    public static func getResourceBundle() -> Bundle {
        guard let resourcePath =
            Bundle.main.path(forResource: "TemplateSDKResources", ofType: "bundle")
            ?? Bundle(for: TemplateUtils.self).path(forResource: "TemplateSDKResources", ofType: "bundle")
        else {
            fatalError("TemplateSDKResources not found")
        }
        return  Bundle(path: resourcePath)!
    }
    
    public static func fieldErrorsToString(_ fieldErrors: [(String, String)]) -> String {
        var errorMessage = ""
        
        for (i, fieldError) in fieldErrors.enumerated() {
            errorMessage += "\(fieldError.0): \(fieldError.1)"
            
            if i + 1 != fieldErrors.count {
                errorMessage += "\n"
            }
        }
        
        return errorMessage
    }
    
    public static func showAlert(from fieldErrors: [(String, String)], presentIn viewController: UIViewController) {
        let alert = UIAlertController(
            title: "Validation Error",
            message: fieldErrorsToString(fieldErrors),
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        viewController.present(alert, animated: true, completion: nil)
    }
    
    /**
     Call this method in the AppDelegate application didFinishLaunchingWithOptions
     if the user is logged in or can receive notifications without be logged in
     
     Call this method in login/signup view controller
     
     Call this method if the user activates the notifications manually
     */
    public static func initNotifications(_ delegate: UNUserNotificationCenterDelegate?, _ resultHandler: ((Bool) -> Void)? = nil) {
        if #available(iOS 10.0, *) {
           // For iOS 10.0 +
            
            if let delegate = delegate {
                UNUserNotificationCenter.current().delegate = delegate
            }
            
            isNotificationsAuthorized{ (isAuthorized) in
                if isAuthorized {
                    self.initNotificationAuthorized()
                    
                    TemplateUserDefaults.setNotificationStatus(enabled: true)
                    resultHandler?(true)
                } else {
                    let center = UNUserNotificationCenter.current()
                    center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                        if granted {
                            DispatchQueue.main.async(execute: {
                                self.initNotificationAuthorized()
                                TemplateUserDefaults.setNotificationStatus(enabled: true)
                                resultHandler?(true)
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                TemplateUserDefaults.setNotificationStatus(enabled: false)
                                resultHandler?(false)
                            })
                        }
                    }
                }
            }
        }else{
            // Below iOS 10.0
            let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            
            TemplateUserDefaults.setNotificationStatus(enabled: true)
            resultHandler?(true)

            //or
            //UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    public static func initNotificationAuthorized() {
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    /**
     Call this method if the user disables notifications manually
     */
    public static func disableNotifications() {
        TemplateUserDefaults.setNotificationStatus(enabled: false)
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    public static func isNotificationsAuthorized(_ resultHandler: @escaping (Bool) -> Void) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                var isAuthorized = false
                switch settings.authorizationStatus {
                case .notDetermined:
                    break
                case .denied:
                    break
                case .authorized:
                    isAuthorized = true
                    break
                case .provisional:
                    isAuthorized = true
                    break
                case .ephemeral:
                    isAuthorized = true
                    break
                @unknown default:
                    break
                }

                DispatchQueue.main.async {
                    resultHandler(isAuthorized)
                }
            }
        } else {
            resultHandler(UIApplication.shared.isRegisteredForRemoteNotifications)
        }
    }
    
    public static func isNotificationsEnabled() -> Bool {
        return UIApplication.shared.isRegisteredForRemoteNotifications
    }
    
    /**
     Call this method from application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
     */
    public static func getNotificationDeviceTokenFromData(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data,
        _ storeDeviceToken: Bool = true,
        _ updateDeviceTokenOnRemote: Bool = true
    ) -> String {
        let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        let accessToken = TemplateUserDefaults.sharedInstance.user?.token
        
        TemplateNetworkUtils.defaultHeaders = ["Authorization": "Bearer \(accessToken ?? "")"]
        
        if storeDeviceToken {
            TemplateUserDefaults.setNotificationDeviceToken(deviceToken: tokenString)
        }
        
        if updateDeviceTokenOnRemote {
            if TemplateUserDefaults.isUserLoggedIn() {
                TemplateUser.updateDeviceToken(
                    url: TemplateInstance.API_URL + TemplateInstance.UPDATE_DEVICE_TOKEN_URL,
                    deviceToken: tokenString,
                    success: {
                       
                    },
                    failure: { (errorMessage, error) in
                        print("Failed to update token")
                    })
            }
        }
        
        return tokenString
    }
    
    public static func updatToken(tokenString:String,_ storeDeviceToken: Bool = true) {
        
        let accessToken = TemplateUserDefaults.sharedInstance.user?.token
        if storeDeviceToken {
            TemplateUserDefaults.setNotificationDeviceToken(deviceToken: tokenString)
        }
        
        TemplateNetworkUtils.defaultHeaders = ["Authorization": "Bearer \(accessToken ?? "")"]
        
        if TemplateUserDefaults.isUserLoggedIn() {
            TemplateUser.updateDeviceToken(
                url: TemplateInstance.API_URL + TemplateInstance.UPDATE_DEVICE_TOKEN_URL,
                deviceToken: tokenString,
                success: {
                   print("succesfully updated device Token")
                },
                failure: { (errorMessage, error) in
                    print("Failed to update token ==>",error.debugDescription)
                })
        }
    }
    
    public static func deleteAccount() {
        
        let accessToken = TemplateUserDefaults.sharedInstance.user?.token
       
        TemplateNetworkUtils.defaultHeaders = ["Authorization": "Bearer \(accessToken ?? "")"]
        
        if TemplateUserDefaults.isUserLoggedIn() {
            let name = TemplateUserDefaults.sharedInstance.user?.name
            let email = "deleted_" + (TemplateUserDefaults.sharedInstance.user?.email)!
            TemplateUser.updateProfile(
                url: TemplateInstance.API_URL + TemplateProfile.updateProfileUrl,
                name: name,
                email: email,
                success: {
                   print("succesfully updated user")
                },
                failure: { (errorMessage, error) in
                    print("Failed to update token ==>",error.debugDescription)
                })
        }
    }
    
    
}

extension UIViewController {
    func removeBackButtonText() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    }
    
    public func showIndicator(_ title: String = "", _ description: String = "", _ interactionEnabled: Bool = false) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        indicator.label.text = title
        indicator.isUserInteractionEnabled = interactionEnabled
        indicator.detailsLabel.text = description
        indicator.show(animated: true)
    }
    
    public func hideIndicator() {
        MBProgressHUD.hide(for: self.view, animated: true)
        UIApplication.shared.endIgnoringInteractionEvents()
    }
}


