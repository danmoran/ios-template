//
//  ValidationExtensions.swift
//  TemplateSDK
//
//  Created by Admin on 06/04/21.
//

import Foundation
import UIKit

extension UITextField {
    func validator() -> Validator {
        return Validator(text: self.text ?? "")
    }
    
    func setValidator(validator: Validator) -> Validator {
        return validator.setText(self.text ?? "")
    }
}

extension UITextView {
    func validator() -> Validator {
        return Validator(text: self.text ?? "")
    }
    
    func setValidator(validator: Validator) -> Validator {
        return validator.setText(self.text ?? "")
    }
}

extension String {
    func validator() -> Validator {
        return Validator(text: self)
    }
    
    func setValidator(validator: Validator) -> Validator {
        return validator.setText(self)
    }
}
