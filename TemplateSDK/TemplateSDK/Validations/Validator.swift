//
//  Validations.swift
//  TemplateSDK
//
//  Created by Admin on 06/04/21.
//

import Foundation

public class Validator {
    init(text: String) {
        self.text = text
    }
    
    public var text: String
    
    public var isValid = true
    
    public var errorMessage: String = ""
    
    public var errorCallback: ((_ message: String) -> Void)? = nil

    public var successCallback: (() -> Void)? = nil
    
    public var ruleList: [BaseRule] = []
    
    public func validate() -> Bool {
        isValid = true
        
        for rule in ruleList {
            if !rule.validate(text: text) {
                setError(rule.getErrorMessage())
                break
            }
        }
        
        if isValid {
            successCallback?()
        } else {
            errorCallback?(errorMessage)
        }
        
        return isValid
    }
    
    public func setText(_ text: String) -> Validator {
        self.text = text
        
        return self
    }
    
    public func setError(_ message: String) {
        isValid = false
        errorMessage = message
    }
    
    public func addRule(_ rule: BaseRule) -> Validator {
        ruleList.append(rule)
        return self
    }
    
    public func addErrorCallback(_ callback: @escaping (_ message: String) -> Void) -> Validator {
        errorCallback = callback
        return self
    }

    public func addSuccessCallback(_ callback: @escaping () -> Void) -> Validator {
        successCallback = callback
        return self
    }
    
    
    // Validations
    
    public func nonEmpty(errorMsg: String? = nil) -> Validator {
        let rule = errorMsg != nil ? NonEmptyRule(errorMsg: errorMsg!) : NonEmptyRule()
        return addRule(rule)
    }
    
    public func minLength(length: Int, errorMsg: String? = nil) -> Validator {
        let rule: MinLengthRule = errorMsg != nil ? MinLengthRule(_errorMsg: errorMsg!, minLength: length) : MinLengthRule(minLength: length)
        return addRule(rule)
    }
    
    public func maxLength(length: Int, errorMsg: String? = nil) -> Validator {
        let rule = errorMsg != nil ? MaxLengthRule(_errorMsg: errorMsg!, maxLength: length) : MaxLengthRule(maxLength: length)
        return addRule(rule)
    }
    
    public func validEmail(errorMsg: String? = nil) -> Validator {
        let rule = errorMsg != nil ? EmailRule(errorMsg: errorMsg!) : EmailRule()
        return addRule(rule)
    }
    
    public func validNumber(errorMsg: String? = nil) -> Validator {
        let rule = errorMsg != nil ? IsNumberRule(errorMsg: errorMsg!) : IsNumberRule()
        return addRule(rule)
    }
    
    public func greaterThan(number: NSNumber, errorMsg: String? = nil) -> Validator {
        let rule = errorMsg != nil ? GreaterThanRule(_errorMsg: errorMsg!, target: number) : GreaterThanRule(target: number)
        return addRule(rule)
    }
    
    public func lessThan(number: NSNumber, errorMsg: String? = nil) -> Validator {
        let rule = errorMsg != nil ? LessThanRule(_errorMsg: errorMsg!, target: number) : LessThanRule(target: number)
        return addRule(rule)
    }
    
    public func regex(pattern: String, errorMsg: String? = nil) -> Validator {
        let rule = errorMsg != nil ? RegexRule(pattern: pattern, errorMsg: errorMsg!) : RegexRule(pattern: pattern)
        return addRule(rule)
    }
}
