//
//  BaseRule.swift
//  TemplateSDK
//
//  Created by Admin on 06/04/21.
//

import Foundation

public protocol BaseRule {
    func validate(text: String) -> Bool
    func getErrorMessage() -> String
    mutating func setError(msg: String)
}
