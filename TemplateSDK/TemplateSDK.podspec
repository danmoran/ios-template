#
# Be sure to run `pod lib lint Another.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TemplateSDK'
  s.version          = '0.1.0'
  s.summary          = 'Template for Tapcrew development projects.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
	Common features, widgets and controllers for internal Tapcrew usage
                       DESC

  s.homepage         = 'www.tapcrew.com'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'proprietary', :file => 'LICENSE' }
  s.author           = 'Tapcrew'
  s.source           = { :git => 'https://github.com/Seqster/Seqster-SDK-PODS.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'

   s.source_files = 'TemplateSDK/**/*.{h,swift}'
#  s.vendored_frameworks = "./outputs/TemplateSDK.xcframework"
#  s.dependency 'TPKeyboardAvoiding', '1.3.5'
  s.dependency 'Alamofire', '~> 5.4'
#  s.dependency 'Charts', '~> 3.6'
  s.dependency 'MBProgressHUD', '~> 1.2.0'
#  s.dependency 'PushNotifications', '~> 3.0.4'
  s.resource_bundles = {
    'TemplateSDKResources' => ['TemplateSDKResources/**/*.{json,png,storyboard,otf,xib,xcassets}']
  }

  s.public_header_files = 'TemplateDemo/SeqsterSDK.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
